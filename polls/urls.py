from django.urls import path

from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name = 'index'),
    path('<int:pk>/', views.DetailView.as_view(), name = 'detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name = 'results'),
    path('<int:question_id>/vote/', views.vote, name = 'vote'),
    path('<int:pk>/reason', views.ReasonsView.as_view(), name = 'newname'),
    path('uploading', views.model_form_upload, name = 'uploader')
]